﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace batcheo
{
    public partial class Form1 : Form
    {
        string ToPath;
        string FromPath;
        string connectionString = "Data Source=ARIELTABARDILLO\\SQLEXPRESS;Initial Catalog=Batching;Integrated Security=True";
        SqlConnection con;
        DateTime BeginTime;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(connectionString);
            con.Open();
            addListItem("File Name","Status");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BeginTime = DateTime.Now;
            ReadXml();
            VerifiyDirectoryExists(ToPath);
            
            if (Directory.Exists(FromPath))
            {
                var Files = Directory.GetFiles(FromPath).Where(s => ".sem".Contains(Path.GetExtension(s)));

                string saveRoute = ToPath;
                var date = DateTime.Now;

                //folders on ToPath
                saveRoute += "\\" + date.Year;
                VerifiyDirectoryExists(saveRoute);

                saveRoute += "\\" + date.DayOfYear;
                VerifiyDirectoryExists(saveRoute);

                //folders on FromPath/DONE
                VerifiyDirectoryExists(FromPath + "\\DONE");
                VerifiyDirectoryExists(FromPath + "\\ERROR");


                foreach (var file in Files)
                {
                    string FileName = Path.GetFileNameWithoutExtension(file);
                    string zipPath = @FromPath + "\\" + FileName + ".zip";
                    string pathError = @FromPath + "\\ERROR\\"+date.DayOfYear;
                    string pathDone = @FromPath + "\\DONE\\" + date.DayOfYear;
                    var FileSem = "\\" + FileName + ".sem";
                    var FileZip = "\\" + FileName + ".zip";

                    if (!Directory.Exists(saveRoute + "\\" + FileName))
                    {
                        //comienza el batheo de un archivo
                        InsertTracking(FileName + ".zip", DateTime.Now, "PRC", "");

                        if (File.Exists(zipPath))
                        {
                            VerifiyDirectoryExists(pathDone);
                            VerifiyDirectoryExists(saveRoute + "\\" + FileName);
                            try
                            {
                                ZipFile.ExtractToDirectory(zipPath, saveRoute + "\\" + FileName);
                                File.Copy(@FromPath + FileZip, pathDone + FileZip);
                                File.Copy(@FromPath + FileSem, pathDone + FileSem);

                                String[] images = Directory.GetFiles(saveRoute + "\\" + FileName);

                                foreach (var item in images)
                                {
                                    InsertImage( FileName+".zip", Path.GetFileName(item));
                                }

                                EditTracking(FileName + ".zip", "CMP", "OK");
                                addListItem(FileName, "OK");
                            }
                            catch (Exception exc)
                            {
                                Directory.Delete(saveRoute + "\\" + FileName);
                                VerifiyDirectoryExists(pathError);
                                if (!File.Exists(pathError + FileSem))
                                {
                                    File.Copy(@FromPath + FileZip, pathError + FileZip);
                                    File.Copy(@FromPath + FileSem, pathError + FileSem);
                                    EditTracking(FileName + ".zip", "ERR", exc.Message);
                                    addListItem(FileName, "Error on stracting the files from zip");
                                }
                                else
                                {
                                    addListItem(FileName, "File extracted before zip");
                                    AlertMessage("File '" + FileName + ".zip' was extracted before ",
                                         "File Extracted Before", MessageBoxIcon.Error);
                                }
                            }
                        }
                        else
                        {
                            //Sem has no pair
                            VerifiyDirectoryExists(pathError);
                            if (!File.Exists(pathError + FileSem))
                            {
                                File.Copy(@FromPath + FileSem, pathError + FileSem);
                                EditTracking(FileName + ".zip", "ERR", "Error the sem file has no zip pair");
                                addListItem(FileName, "Error the sem file has no zip pair");
                            }
                            else
                            {
                                addListItem(FileName, "File extracted before zip");
                                AlertMessage("File '" + FileName + ".zip' was extracted before ",
                                     "File Extracted Before", MessageBoxIcon.Error);
                            }
                        }
                    }
                    else
                    {
                        addListItem(FileName, "File extracted before zip");
                        AlertMessage("File '" + FileName + ".zip' was extracted before ",
                             "File Extracted Before", MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                AlertMessage("From directory doesn't exists,Verify the From route exists",
                    "From directory doesn't exists", MessageBoxIcon.Error);
            }
        }

        public void ReadXml()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML Files (*.xml)|*.xml";
            openFileDialog.FilterIndex = 0;
            openFileDialog.DefaultExt = "xml";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (!String.Equals(Path.GetExtension(openFileDialog.FileName), ".xml", StringComparison.OrdinalIgnoreCase))
                {
                    AlertMessage(
                        "The type of the file is not supported by this application, select an XML file.",
                        "Invalid File Type", MessageBoxIcon.Error
                        );
                    
                }
                else
                {
                    var xml = XDocument.Load(openFileDialog.FileName);

                    this.ToPath = ((from c in xml.Descendants("configuration")
                                    select c.Element("ToPath").Value).First()).Replace(@"\\", @"\");

                    this.FromPath = ((from c in xml.Descendants("configuration")
                                      select c.Element("FromPath").Value).First()).Replace(@"\\", @"\");
                }
            }
        }

        public void VerifiyDirectoryExists(string route)
        {
            if (!Directory.Exists(route))
            { Directory.CreateDirectory(route); }
        }

        public void AlertMessage(string msg, string title, MessageBoxIcon c)
        {
            MessageBox.Show(msg,title,MessageBoxButtons.OK,c);
        }

        public void InsertImage(string ZipName, string ImageName )
        {
            string q = "insert into Image(ZipName,ImageName) values('"+ZipName+"','"+ImageName+"')";
            SqlCommand cmd = new SqlCommand(q, con);
            InsertDB(cmd);
        }

        public void InsertTracking(string ZipName, DateTime BatchingStart, string Status, string Comments)
        {
            string q = "insert into Tracking(ZipName,BatchingStart_DateTime,Status,Comments) " +
                "values('" + ZipName + "', @Date ,'" + Status + "','" + Comments + "')";
            
            SqlCommand cmd = new SqlCommand(q, con);
            cmd.Parameters.AddWithValue("@Date", DateTime.Now);

            InsertDB(cmd);
        }

        public void EditTracking(string ZipName,string status, string comment)
        {
            string q = "UPDATE Tracking SET BatchingEnd_DateTime = @Date, Status = @status, Comments= @Comments " +
                "Where Status = 'PRC' AND ZipName = '" + ZipName + "'";

            SqlCommand cmd = new SqlCommand(q, con);
            cmd.Parameters.AddWithValue("@Date", DateTime.Now);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@Comments", comment);
            InsertDB(cmd);
        }

        public void InsertDB(SqlCommand cmd)
        {
            if (con.State == ConnectionState.Open)
            {
                cmd.ExecuteNonQuery();
            }
            else
            {
                AlertMessage("Check your database Connection",
                        "DB connection Error", MessageBoxIcon.Error);
            }
        }

        public void addListItem(string name, string Status)
        { listBox1.Items.Add(String.Format("{0}\t\t\t{1}", name, Status)); }
    }
}
