﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models.Dtos;
using WebApplication1.Services;

namespace WebApplication1.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrackingController : Controller
    {
        TrackingService _trackingService = new TrackingService();

        // GET: api/Tracking
        [HttpGet]
        public IEnumerable<TrackingsByDayDto> GetAll()
        {
            var x = _trackingService.GetAll();
            return x;
        }
        [HttpGet("[action]")]
        public IEnumerable<TrackingDto> GetTrackings(int d,int m, int y)
        {
            DateTime date = new DateTime(y, m, d);
            var x = _trackingService.GetTrackings(date);
            return x;
        }

        [HttpGet("[action]")]
        public IEnumerable<ImageDto> GetImages(string zipname)
        {
            var x = _trackingService.GetImages(zipname);
            return x;
        }
    }
}
    