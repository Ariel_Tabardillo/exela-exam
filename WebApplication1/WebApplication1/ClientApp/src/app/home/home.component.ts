import { Component } from '@angular/core';
import { TrackingByDayModel } from "../Models/TrackingByDay"
import { TrackingServiceService } from "../Services/tracking-service.service"


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  public trackings: TrackingByDayModel[];
  constructor(private _TrackingService: TrackingServiceService) { }

  ngOnInit() {
    this._TrackingService.getAllTrackings().subscribe((data) => {
      this.trackings = data;
    });
  }

  GetDate(d: Date): string {
    var date = new Date(d);

    var y = date.getFullYear();
    var m = date.getMonth()+1;
    var dd = date.getDate();
    
    return m+"-"+dd+"-"+y;
  }
}
