import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TrackingServiceService } from "../Services/tracking-service.service"
import { TrackingModel } from "../Models/TrackingModel"

@Component({
  selector: 'app-report-l2',
  templateUrl: './report-l2.component.html',
  styleUrls: ['./report-l2.component.css']
})
export class ReportL2Component implements OnInit {

  public date_original: string;
  public title_date: Date;
  public date: string;
  public trackings: TrackingModel[];

  constructor(
    private route: ActivatedRoute,
    private _TrackingService: TrackingServiceService
  ) { }

  ngOnInit() {
    this.date = this.route.snapshot.paramMap.get('date');
    this.date_original = this.route.snapshot.paramMap.get('date');
    this.date = this.date.replace("-", "/");
    this.date = this.date.replace("-", "/");
    this.title_date = new Date(this.date);
    this._TrackingService.getTrackings(this.date).subscribe((data) => {
      this.trackings = data;
      if (this.trackings.length <= 0) {
        window.location.href = '.'
      }
      console.log(data);
    });
  }
}
