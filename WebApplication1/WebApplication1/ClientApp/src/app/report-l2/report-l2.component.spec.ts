import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportL2Component } from './report-l2.component';

describe('ReportL2Component', () => {
  let component: ReportL2Component;
  let fixture: ComponentFixture<ReportL2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportL2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportL2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
