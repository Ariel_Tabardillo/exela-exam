import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TrackingServiceService } from "../Services/tracking-service.service"
import { ImageModel } from "../Models/ImageModel"

@Component({
  selector: 'app-report-l3',
  templateUrl: './report-l3.component.html',
  styleUrls: ['./report-l3.component.css']
})
export class ReportL3Component implements OnInit {

  public images: ImageModel[];
  public zipName: string;

  constructor(
    private route: ActivatedRoute,
    private _TrackingService: TrackingServiceService
  ) { }

  ngOnInit() {
    this.zipName = this.route.snapshot.paramMap.get('ZipName');
    this._TrackingService.getImages(this.zipName).subscribe((data) => {
      this.images = data;
      if (this.images.length <= 0) {
        window.location.href = '.'
      }
      console.log(data);
    });

  }

}
