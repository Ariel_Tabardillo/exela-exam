import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportL3Component } from './report-l3.component';

describe('ReportL3Component', () => {
  let component: ReportL3Component;
  let fixture: ComponentFixture<ReportL3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportL3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportL3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
