import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TrackingByDayModel } from "../Models/TrackingByDay"
import { TrackingModel } from "../Models/TrackingModel"
import { ImageModel } from "../Models/ImageModel"
import { Observable } from "rxjs/observable"

@Injectable()
export class TrackingServiceService {

  private _url: string = './api/Tracking';

  constructor(private http: HttpClient) { }

  getAllTrackings(): Observable<TrackingByDayModel[]> {
    return this.http.get<TrackingByDayModel[]>(this._url);
  }

  getTrackings(date: string): Observable<TrackingModel[]> {
    var d = new Date(date);
    let params = new HttpParams().set('d', "" + d.getDate()).set('m', "" + (d.getMonth()+1) ).set('y', "" + d.getFullYear());
    return this.http.get<TrackingModel[]>(this._url + "/GetTrackings/", { params: params });
  }

  getImages(zipName: string): Observable<ImageModel[]> {
    let params = new HttpParams().set('zipname', zipName);
    return this.http.get<ImageModel[]>(this._url + "/GetImages", { params: params });
  }

  

}
