import { TestBed, inject } from '@angular/core/testing';

import { TrackingServiceService } from './tracking-service.service';

describe('TrackingServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrackingServiceService]
    });
  });

  it('should be created', inject([TrackingServiceService], (service: TrackingServiceService) => {
    expect(service).toBeTruthy();
  }));
});
