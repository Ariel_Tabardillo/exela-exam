import { ImageModel } from './ImageModel'

export interface TrackingModel {
  id: number,
  zipName: string,
  startDateTime: Date,
  endDateTime: Date,
  status: string,
  comments: string,
  images: ImageModel[]
}
