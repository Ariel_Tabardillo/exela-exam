import { TrackingModel } from './TrackingModel'

export interface TrackingByDayModel {
  batchDay: Date,
  Images: TrackingModel[]
}
