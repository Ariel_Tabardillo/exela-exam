import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { TrackingServiceService } from "./Services/tracking-service.service";
import { ReportL2Component } from './report-l2/report-l2.component';
import { ReportL3Component } from './report-l3/report-l3.component'

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    ReportL2Component,
    ReportL3Component,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'secondlevel/:date', component: ReportL2Component },
      { path: 'thirdlevel/:ZipName', component: ReportL3Component },
    ])
  ],
  providers: [TrackingServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
