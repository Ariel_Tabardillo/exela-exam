﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplication1.Models;
using WebApplication1.Models.Dtos;

namespace WebApplication1.Services
{
    public class TrackingService
    {
        private BatchingContext db = new BatchingContext();
        private List<DateTime> days;

        public List<TrackingsByDayDto> GetAll()
        {
            List <TrackingsByDayDto> res = new List<TrackingsByDayDto>();
            days = new List<DateTime>();
            
            var entities = db.Set<Tracking>().Select(item => new TrackingDto
            {
                Id = item.Id,
                ZipName = item.ZipName,
                Status = item.Status,
                BatchingStartDateTime = item.BatchingStartDateTime,
                BatchingEndDateTime = item.BatchingEndDateTime,
                Comments = item.Comments,
                Images = db.Set<Image>().Where(x => x.ZipName == item.ZipName).Count()
            });

            foreach (var item in entities)
            {
                var d = item.BatchingEndDateTime;
                days.Add( new DateTime(d.Year,d.Month,d.Day,0,0,0) );
            }
            IEnumerable<DateTime> distinctdays = days.Distinct();

            foreach (var day in distinctdays)
            {
                DateTime EndDay = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59);

                var filter = entities.Where(x => x.BatchingEndDateTime >= day && x.BatchingEndDateTime <= EndDay);

                res.Add(new TrackingsByDayDto { BatchDay =day, Trackings = filter.Count() } );
            }
            
            return res;
        }

        public IEnumerable<TrackingDto> GetTrackings(DateTime date)
        {
            DateTime EndDay = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
            DateTime StartDay = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);

            var entities = db.Set<Tracking>().Where(x => x.BatchingEndDateTime >= StartDay && x.BatchingEndDateTime <= EndDay)
            .Select(item => new TrackingDto
            {
                Id = item.Id,
                ZipName = item.ZipName,
                Status = item.Status,
                BatchingStartDateTime = item.BatchingStartDateTime,
                BatchingEndDateTime = item.BatchingEndDateTime,
                Comments = item.Comments,
                Images = db.Set<Image>().Where(x => x.ZipName == item.ZipName).Count()
            });

            return entities;
        }

        public IEnumerable<ImageDto> GetImages(string ZipName)
        {
            var images = db.Set<Image>().Where(x => x.ZipName == ZipName)
                    .Select(y => new ImageDto
                    {
                        Id = y.Id,
                        ZipName = y.ZipName,
                        ImageName = y.ImageName
                    });
            return images;
        }

    }
}
