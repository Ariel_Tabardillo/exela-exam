﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Models
{
    public partial class BatchingContext : DbContext
    {
        public BatchingContext()
        {
        }

        public BatchingContext(DbContextOptions<BatchingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Image> Image { get; set; }
        public virtual DbSet<Tracking> Tracking { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=ARIELTABARDILLO\\SQLEXPRESS;Database=Batching;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>(entity =>
            {
                entity.Property(e => e.ImageName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ZipName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tracking>(entity =>
            {
                entity.Property(e => e.BatchingEndDateTime)
                    .HasColumnName("BatchingEnd_DateTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BatchingStartDateTime)
                    .HasColumnName("BatchingStart_DateTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ZipName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });
        }
    }
}
