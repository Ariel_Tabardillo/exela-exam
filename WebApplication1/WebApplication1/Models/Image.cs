﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public partial class Image
    {
        public int Id { get; set; }
        public string ZipName { get; set; }
        public string ImageName { get; set; }
    }
}
