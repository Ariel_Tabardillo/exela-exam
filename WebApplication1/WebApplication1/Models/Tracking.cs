﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public partial class Tracking
    {
        public int Id { get; set; }
        public string ZipName { get; set; }
        public DateTime BatchingStartDateTime { get; set; }
        public DateTime BatchingEndDateTime { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
    }
}
