﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.Dtos
{
    public class TrackingDto
    {
        public int Id { get; set; }
        public string ZipName { get; set; }
        public DateTime BatchingStartDateTime { get; set; }
        public DateTime BatchingEndDateTime { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }

        public int Images { get; set; }
    }
}
