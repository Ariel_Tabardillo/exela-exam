﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.Dtos
{
    public class TrackingsByDayDto
    {
        public DateTime BatchDay { get; set; }
        public int Trackings { get; set; }
    }
}
