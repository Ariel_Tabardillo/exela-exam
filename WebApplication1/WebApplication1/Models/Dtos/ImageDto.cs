﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.Dtos
{
    public class ImageDto
    {
        public int Id { get; set; }
        public string ZipName { get; set; }
        public string ImageName { get; set; }
    }
}
